=======================
Absolunet Domains Lists
=======================

------------
Translations
------------

- `Version française <README.fr.rst>`_

------------
Files Format
------------

- Each files contains a list of domain names
- The ``#`` character defines the start of a comment
- The ``;`` character is used as sepratator
- An domain name starting with an asterisk followed by a dot ``.`` will match any subdomains
- Empty lines are ignored

The data is structured as follow:

:Domain Name: The domain information to compare
:Report: ``true`` (active) or ``false`` (inactive), used to allow the reporting of those elements to the central server
:Comment: The comment to show when an entry is matched

Examples of valid entries:

::

  test.com; true; My comment -- author@web.com
  *.test2.com; false; My comment -- author@web.com
  test3.com; true
  test4.com

- This is invalid: ``*.subdomain.test2.com``
- This is invalid: ``test.*.test2.com``

.. note::

  It is common to list the author of a comment in order to know who to contact if you need more information.

  The top-most domain from the list takes precedence over any other matching domains.

  You can ignore the last 2 values, the reporting will default to true.
