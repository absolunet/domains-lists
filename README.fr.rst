============================
Liste des domaines Absolunet
============================

-----------
Traductions
-----------

- `English version <README.rst>`_

-------------------
Format des fichiers
-------------------

- Chaque fichiers contiennent une liste de noms de domaines
- Le caractère ``#`` dénote un commentaire
- Le caractère ``;`` sert de séparateur des données
- Tout noms de domaines qui commencent par un astérisque et est immédiatement suivis d'un point ``.`` vont correspondre à tout les sous-domaines
- Les lignes vide sont ignoré

La structure des données est tel que suivis:

:Nom de domaine: Le nom de domaine à comparer pour trouver des correspondances
:Rapport: ``true`` (actif) ou ``false`` (inactif), sert à activer les rapports vers le serveur central pour ces correspondances
:Commentaire: Le commentaire à afficher sur la correspondance

Exemple d'entrées valides:

::

  test.com; true; Mon commentaire -- author@web.com
  *.test2.com; false; Mon commentaire -- author@web.com
  test3.com; true
  test4.com

- Ceci est invalide: ``*.subdomain.test2.com``
- Ceci est invalide: ``test.*.test2.com``

.. note::

  Il est commun d'ajouter l'auteur d'un commentaire pour pouvoir avoir une personne à contacter s'il y à besoin d'avantage d'informations.

  Le premier domaine qui correspond parmis la liste est l'entrée qui est utilisé par les logiciels.

  Vous pouvez ignorer les 2 dernière valeures, le rapport est activé par défaut.
